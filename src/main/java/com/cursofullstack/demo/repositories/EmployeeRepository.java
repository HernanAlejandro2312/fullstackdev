package com.cursofullstack.demo.repositories;

import com.cursofullstack.demo.domain.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {


}
