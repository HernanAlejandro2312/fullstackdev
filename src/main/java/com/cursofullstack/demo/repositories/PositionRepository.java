package com.cursofullstack.demo.repositories;

import com.cursofullstack.demo.domain.Position;
import org.springframework.data.repository.CrudRepository;

public interface PositionRepository extends CrudRepository<Position, Long> {
}
