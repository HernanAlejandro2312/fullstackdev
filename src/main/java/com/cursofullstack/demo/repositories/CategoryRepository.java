package com.cursofullstack.demo.repositories;

import com.cursofullstack.demo.domain.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}
