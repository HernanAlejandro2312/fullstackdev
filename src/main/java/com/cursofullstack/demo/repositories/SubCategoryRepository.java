package com.cursofullstack.demo.repositories;

import com.cursofullstack.demo.domain.SubCategory;
import org.springframework.data.repository.CrudRepository;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {
}
