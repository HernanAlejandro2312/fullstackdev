package com.cursofullstack.demo.repositories;

import com.cursofullstack.demo.domain.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
