package com.cursofullstack.demo.repositories;

import com.cursofullstack.demo.domain.Contract;
import org.springframework.data.repository.CrudRepository;

public interface ContractRepository extends CrudRepository<Contract, Long> {
}
